package Figure;

import java.util.Random;

public class Square extends Figure implements Moveable{

    public Square(int x, int y) {
        super(x, y);
    }

    @Override
    public int getX() {
        return super.getX();
    }

    @Override
    public int getY() {
        return super.getY();
    }

    @Override
    void getPerimeter() {

    }

    @Override
    public void move(int x, int y) {
        Random random = new Random();
        Random random1 = new Random();

        x = random.nextInt(100) + getX();
        y = random.nextInt(100) + getY();
        System.out.println("Квадрат сместился на координаты: " + x + ", " + y);

    }
}
