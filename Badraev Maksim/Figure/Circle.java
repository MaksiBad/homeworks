package Figure;

import java.util.Random;

public class Circle extends Ellipse implements Moveable{

    public Circle(int x, int y) {
        super(x, y);
    }

    @Override
    public int getX() {
        return super.getX();
    }

    @Override
    public int getY() {
        return super.getY();
    }

    @Override
    public void move(int x, int y) {
        Random random = new Random();
        Random random1 = new Random();

        x = random.nextInt(100) + getX();
        y = random.nextInt(100) + getY();
        System.out.println("Круг сместился на координаты: " + x + ", " + y);
    }
}
