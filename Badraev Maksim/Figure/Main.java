package Figure;

public class Main {
    public static void main(String[] args) {
        Ellipse ellipse = new Ellipse(23,23);
        ellipse.getPerimeter();
        Rectangle rectangle = new Rectangle(845,845);
        rectangle.getPerimeter();
        Circle circle = new Circle(ellipse.getX(), ellipse.getY());
        Square square = new Square(rectangle.getX(), rectangle.getY());

        if (ellipse.getX() == ellipse.getY())
            circle.move(circle.getX(), circle.getY());
        if (rectangle.getX() == rectangle.getY())
            square.move(square.getX(), square.getY());
    }
}
