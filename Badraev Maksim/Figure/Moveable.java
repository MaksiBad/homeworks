package Figure;

public interface Moveable {

    void move (int x, int y);
}
