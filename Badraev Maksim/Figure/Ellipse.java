package Figure;

public class Ellipse extends Figure{

    public Ellipse(int x, int y) {
        super(x, y);
    }


    @Override
    void getPerimeter() {

        if (getX() != getY()) {

            double p = 2 * Math.PI * Math.sqrt(Math.pow(getX(), 2) + Math.pow(getY(), 2)) / 8;
            System.out.println("Периметр эллипса равен: " + p);
        }
        if (getX() == getY()){

            double p = 2 * Math.PI * getX();
            System.out.println("Периметр круга равен: " + p);

        }
    }

    @Override
    public int getX() {
        return super.getX();
    }

    @Override
    public int getY() {
        return super.getY();
    }
}
