package Figure;

public class Rectangle extends Figure{

    public Rectangle(int x, int y) {
        super(x, y);
    }

    @Override
    public int getX() {
        return super.getX();
    }

    @Override
    public int getY() {
        return super.getY();
    }

    @Override
    void getPerimeter() {
        if (getX() != getY()) {

            double p = 2*getX() + 2*getY();
            System.out.println("Периметр прямоугольника равен: " + p);
        }
        if (getX() == getY()){

            double p = 4 * getX();
            System.out.println("Периметр квадрата равен: " + p);

        }
    }
}
