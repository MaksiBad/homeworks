package JDBC;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.Properties;
public class CrudHumanImpl implements CrudHuman{
    private static Connection con;
    private static final String ALL_FIND = "SELECT * FROM humans";
    private static final String FIND_BY_ID = "SELECT * FROM humans WHERE id = ?";
    private static final String UP_DATE = "UPDATE humans SET name = ? WHERE id = ?";
    private static final String DEL = "DELETE FROM humans WHERE id = ?";
    private static final String CR_HUM = "INSERT INTO humans(name,lastname,patronymic,city,street,house,flat,numberpassport) VALUES (?,?,?,?,?,?,?,?)";
    public CrudHumanImpl() throws SQLException {
        con = createConnection();
    }
    public  static Connection createConnection() throws SQLException {
        Properties properties = new Properties();
        try {
            FileInputStream fileInputStream = new FileInputStream("src/main/resources/db.properties");
            properties.load(fileInputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return DriverManager.getConnection(
                properties.getProperty("db.url"),
                properties.getProperty("db.user"),
                properties.getProperty("db.password"));
    }
    @Override
    public ResultSet getAllHumans() throws SQLException {
        PreparedStatement preparedStatement = con.prepareStatement(ALL_FIND);
        return  preparedStatement.executeQuery();
    }
    @Override
    public Human getHumanById(int id) throws SQLException {
        PreparedStatement preparedStatement = con.prepareStatement(FIND_BY_ID);
        preparedStatement.setInt(1,id);
        ResultSet resultSet = preparedStatement.executeQuery();
        resultSet.next();
        return new Human(resultSet.getInt("id"),
                resultSet.getString("name"),
                resultSet.getString("lastname"),
                resultSet.getString("patronymic"),
                resultSet.getString("city"),
                resultSet.getString("house"),
                resultSet.getString("street"),
                resultSet.getString("flat"), resultSet.getString("numberpassport"));
    }
    @Override
    public void createHuman(Human human) throws SQLException{
        PreparedStatement preparedStatement = con.prepareStatement(CR_HUM);
        preparedStatement.setString(1,human.getName());
        preparedStatement.setString(2, human.getLastName());
        preparedStatement.setString(3, human.getPatronymic());
        preparedStatement.setString(4, human.getCity());
        preparedStatement.setString(5, human.getStreet());
        preparedStatement.setString(6, human.getHouse());
        preparedStatement.setString(7, human.getFlat());
        preparedStatement.setString(8, human.getNumberPassport());
        preparedStatement.executeUpdate();
        System.out.println(human);
    }
    @Override
    public void updateHuman(Human human) throws SQLException{
        human = new Human(human.getId(), human.getName(), human.getLastName(), human.getPatronymic(),
                human.getCity(), human.getStreet(), human.getHouse(), human.getFlat(), human.getNumberPassport());
        PreparedStatement preparedStatement = con.prepareStatement(UP_DATE);
        preparedStatement.setString(1, human.getName());
        preparedStatement.setInt(2, human.getId());
        preparedStatement.executeUpdate();
        System.out.println(human);
    }
    @Override
    public void deleteHuman(Human human) throws SQLException{
        PreparedStatement preparedStatement = con.prepareStatement(DEL);
        preparedStatement.setInt(1, human.getId());
        preparedStatement.executeUpdate();
    }
}
