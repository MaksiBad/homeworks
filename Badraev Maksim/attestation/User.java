package attestation;

import java.util.Objects;

public class User {
    private int id;
    private String name;
    private String surName;
    private int age;

    public User(int id, String name, String surName, int age) {
        setId(id);
        setName(name);
        setSurName(surName);
        setAge(age);
    }
    public User(String id, String name, String surName, String age) {
        setId(id);
        setName(name);
        setSurName(surName);
        setAge(age);
    }

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    public void setId(String id) {
        this.id = Integer.parseInt(id);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurName() {
        return surName;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
    public void setAge(String age) {
        this.age = Integer.parseInt(age);
    }

    @Override
    public String toString() {
        return id + "|" + name + "|" + surName + "|" + age + "|";
    }
}