package attestation;

import java.io.*;

public class Main {
    public static void main(String[] args) throws IOException {
        UsersRepository usersRepository = new UsersRepository();

        //Объявляем объектные переменные для чтения строк из файла list_People.txt
        // и записи данных с одной из обновленной строкой в файл list_People1.txt
        BufferedReader reader = null;
        BufferedWriter writer = null;

        //Объявляем переменную типа User для того чтобы внести данные в список с помощью строковой переменной line
        User user = null;
        String line = null;
        try {
            //Связываем reader с файлом list_People.txt для чтения через буффер
            reader = new BufferedReader(
                    new FileReader("C:\\Users\\dmitr\\IdeaProjects\\MyProgect\\src\\attestation\\list_People.txt"));

            //Пока считываемая строка из файла не пустая, выполняем тело цикла
            while ((line = reader.readLine()) != null){

                //Создаем массив, элементы которого разделены символом "|"
                String[] temp = line.split("\\|");

                //Заполняем поля user'а (temp[0] = id, temp[1] = name, temp[2] = surName, temp[3] = age)
                user = new User(temp[0],temp[1],temp[2],temp[3]);

                //Выводим списки юзеров в консоль
                System.out.println(user);

                //Добавляем данные в список userArraylist класса UserRepository
                usersRepository.create(user);
            }

        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            try {
                //Закрываем поток input
                reader.close();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        System.out.println("------------------------");
        System.out.println("Юзер с искомым id");

        //Выводим в консоль юзера с выбранным id
        System.out.println(usersRepository.findById(3));

        //Проводим update. Сначала считываем с файла list_People.txt, затем построчно записываем в файл list_People1.txt
        //перед этим находим нужную строку и меняем имя, фамилию и возраст
        try {
            reader = new BufferedReader(
                    new FileReader("C:\\Users\\dmitr\\IdeaProjects\\MyProgect\\src\\attestation\\list_People.txt"));
            writer = new BufferedWriter(
                    new FileWriter("C:\\Users\\dmitr\\IdeaProjects\\MyProgect\\src\\attestation\\list_People1.txt"));
            int i = 1;
            while ((line = reader.readLine()) != null){
                String[] temp = line.split("\\|");
                user = new User(temp[0], temp[1], temp[2], temp[3]);
                if (i == 3){

                    //меняем имя, фамилию, возраст
                    user.setName("Андрей");
                    user.setSurName("Серегин");
                    user.setAge(19);

                    //Записываем изменненую строку в буфер
                    writer.write(String.valueOf(user));
                } else {
                    writer.write(line);
                }

                //Добавляем строку из буфера в файл list_People1.txt
                writer.flush();

                //Переходим к следующей строке
                writer.newLine();
                i++;
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            try {
                //Закрываем потоки IO
                reader.close();
                writer.close();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        //Удаление юзера(не работает).
        // Нужно считывать файл построчно и при нахождении юзера с искомым id заменить строку на пустую
        usersRepository.delete(user.getId());
    }
}
