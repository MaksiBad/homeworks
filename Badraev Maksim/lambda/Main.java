package lambda;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Random random = new Random();
        int[] array = new int[10];

        for (int i = 0; i < array.length; i++)
            array[i] = random.nextInt(20)+1;
        System.out.println("Значение элементов массива");
        System.out.println(Arrays.toString(array));
        System.out.println("********************************");

        System.out.println("Четные элементы массива");
        System.out.println(Arrays.toString(Sequence.filter(array,number -> number % 2 == 0)));
        //System.out.println();
        System.out.println("********************************");

        System.out.println("Элементы массива, сумма цифр которых является четным числом");
        System.out.println(Arrays.toString(Sequence.filter(array,number -> {
            int sum = 0;
            while (number != 0){
                sum = number % 10 + sum;
                number /= 10;
            }
            return sum % 2 == 0;
        })));

        System.out.println("********************************");

        Random random1 = new Random();
        int i = random1.nextInt(200);
        System.out.println("Число: " + i);

        Sequence.filterEvenOdd(i, number -> number % 2 == 0);

        System.out.println();
        System.out.println("********************************");

        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите число: ");
        int digit = scanner.nextInt();

        Sequence.filterPolindrom(digit,number -> number == digit);



       
    }



}
