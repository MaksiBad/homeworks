package lambda;

public class Sequence {

    public static int[] filter(int[] array, ByCondition condition){
        int[] array1 = new int[10];
        for (int i = 0; i < array.length; i++){
            if (condition.isOk(array[i]))
                array1[i] = array[i];

        }
        return array1;
    }

    public static void filterEvenOdd(int number, ByCondition condition){
        int digit;
        while (number != 0){
            digit = number % 10;
            number /= 10;
            if (condition.isOk(digit))
                System.out.print(digit + " четное" + " ");
            else System.out.print(digit + " нечетное" + " ");
        }
    }

    public static void filterPolindrom(int number, ByCondition condition){
        int digit = 0;
        int i = 0;
        int num = number;

        while (number != 0) {
            digit = number % 10;
            i = i * 10 + digit;
            number = number / 10;
        }
        if (condition.isOk(i))
            System.out.println("полиндром");
        else
            System.out.println("не полиндром");
    }

}
