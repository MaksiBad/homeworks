import java.util.Scanner;
public class Minimum {
    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);
        Scanner exit = new Scanner(System.in);
        int a = scanner.nextInt();
        int min = Integer.MAX_VALUE;
        while (a!=-1){
            if (a<min)
                min=a;
	    a = scanner.nextInt();
        }
        System.out.println("Встречено '-1' - признак окончания последовательности");
        System.out.println("Выход из программы, для продолжения нажмите ENTER");
        String ex = exit.nextLine();
	System.out.println("Минимальное число: " + min);
    }
}
