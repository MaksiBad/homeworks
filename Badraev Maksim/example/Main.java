package example;

import java.sql.Array;
import java.util.Arrays;
import java.util.Random;

public class Main {
    public static void main(String[] args) {
        Random random = new Random();
        Human[] humans = new Human[10];
        for (int i = 0; i < humans.length; i++){
            humans[i] = new Human("Name" + i, "lastName" + i, random.nextInt(80));
            System.out.println(humans[i].getName() + ", " + humans[i].getLastName() + ", " + humans[i].getAge());
        }
        System.out.println("***********************************");

        for (int i = 0; i < humans.length; i++){
            for (int j = i+1; j < humans.length; j++){
                if (humans[i].getAge() > humans[j].getAge()){
                    Human count = humans[i];
                    humans[i] = humans[j];
                    humans[j] = count;
                }
            }
            System.out.println(humans[i].getName() + ", " + humans[i].getLastName() + ", " + humans[i].getAge());
        }

    }

}
