package object;

public class Main {
    public static void main(String[] args) {

        Human human = new Human("Вася","Васин","Васильевич",
                "Город Масква","ул. Ленина,",
                "дом, 45,","квартира, 34.","Серия: 0740 Номер: 345891");

        Human human1 = new Human("Петя","Петров","Петрович",
                "Город Казань","ул. Энтузиастов,",
                "дом, 15,","квартира, 5.","Серия: 0740 Номер: 345891");

        Human human2 = new Human("Ваня","Иванов","Иванович",
                "Город Иркутск","ул. Нечаева,",
                "дом, 3,","квартира, 29.","Серия: 0720 Номер: 453728");

        System.out.println(human.toString());
        System.out.println("------------------------------------");
        System.out.println(human1.toString());
        System.out.println("------------------------------------");
        System.out.println(human2.toString());
        System.out.println("------------------------------------");

        System.out.println(human.equals(human1));
        System.out.println(human1.equals(human2));
        System.out.println(human2.equals(human));
        System.out.println("------------------------------------");

        System.out.println(human.hashCode());
        System.out.println(human1.hashCode());
        System.out.println(human2.hashCode());



    }
}
