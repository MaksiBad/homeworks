import java.util.Scanner;

public class Search {
    public static void main(String[] args) {
        int[] array = new int[10];

        Scanner number = new Scanner(System.in);
        Scanner scanner = new Scanner(System.in);

        System.out.print("Введите число: ");
        int n = number.nextInt();

        System.out.println();

        System.out.println("Введите элементы массива: ");
        for (int i = 0; i < array.length; i++)
            array[i] = scanner.nextInt();
        System.out.println("Индекс элемента массива, совпадающий с числом " + n + ": " + functionSearchArray(n, array));
    }

    public static int functionSearchArray(int number, int[] array){
        int count = -1;

        for (int i =0; i < array.length; i++){
            if (number == array[i]) {
                count = i;
                break;
            }
        }
        return count;
    }
}
