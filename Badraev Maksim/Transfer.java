import java.util.Arrays;
import java.util.Scanner;

public class Transfer {
    public static void main(String[] args) {
        int[] array = new int[10];
        int[] copyArray = new int[10];

        Scanner scanner = new Scanner(System.in);
        for (int i = 0; i < array.length; i++)
            array[i] = scanner.nextInt();

        methodTransfer(array, copyArray);
        scanner.close();
    }
    public static void methodTransfer (int[] array, int[] copyArray){
        int count = 0;
        for (int i = 0; i < array.length; i++){
            if (array[i] != 0) {
                copyArray[count] = array[i];
                //System.out.println(copyArray[count]);
                count++;
            }
        }
        System.out.println(Arrays.toString(copyArray));
    }
}
