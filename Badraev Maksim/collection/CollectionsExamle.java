package collection;

import java.util.HashMap;
import java.util.Map;

public class CollectionsExamle {
    public static void main(String[] args) {
        String strings;
        strings = "володя максим толя дятел алеша";
        String[] str = strings.split(" ");
        Map<String, Integer> stringMap = new HashMap<>();
        for (String s : str) {
            if (!stringMap.containsKey(s)){
                stringMap.put(s, 0);
            }
            stringMap.put(s, stringMap.get(s) + 1);
        }
        for (String s : stringMap.keySet())
        {
            System.out.println(s + " - " + stringMap.get(s));
        }
    }
}
