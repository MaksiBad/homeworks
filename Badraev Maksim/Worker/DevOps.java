package Worker;

public class DevOps extends Worker{
    private String work;

    public DevOps(String name, String lastName, String profession, String work) {
        super(name, lastName, profession);
        this.work = work;
    }

    @Override
    public void goToWork() {
        System.out.println(getName() + " " + getLastName() + " " + getProfession());
        System.out.println("Режим работы: " + work);
    }

    @Override
    public void qoToVacation(int days) {
        System.out.println("количество дней отпуска: " + days);
    }
}
