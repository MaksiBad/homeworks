package Worker;

import java.util.Random;

public class Main {
    public static void main(String[] args) {
        Random random = new Random();
        Worker[] workers = new Worker[4];
       workers[0] = new Programmer("Иван", "Иванов: ", "Программист", "Понедельник и Четверг");
       workers[1] = new Tester("Петя", "Петров: ", "Тестировщик", "Вторник и Пятница");
       workers[2] = new SysAdmin("Коля", "Колев: ", "СисАдмин", "Понедельник, Среда, Пятница");
       workers[3] = new SysAdmin("Митя", "Митин: ", "ДевОпс", "С Понедельника по Пятницу");
       for (int i = 0; i < workers.length; i++){
           workers[i].goToWork();
           workers[i].qoToVacation(random.nextInt(20));
           System.out.println("*********************************");
       }
    }
}
