import java.util.Scanner;
public class EvenEndOdd {
    public static void main (String[] args) {
        Scanner scanner = new Scanner(System.in);
        Scanner ex = new Scanner(System.in);
        int a = scanner.nextInt();
        int even = 0;
        int odd = 0;
        while (a != -1) {
            if (a % 2 == 0) {
                even = even +1;
            }
            if (a % 2 != 0)
                odd = odd +1;
            a = scanner.nextInt();
        }
        System.out.println("Встечено окончание ввода последовательности: -1");
        System.out.println("Нажмите ENTER для выхода");
        String e = ex.nextLine();
        System.out.println("Количество чётных чисел: "+even);
        System.out.println("Количество нечётных чисел:" + odd);
    }
}
