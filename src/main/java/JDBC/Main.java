package JDBC;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
public class Main {
    public static void main(String[] args) throws SQLException {
        CrudHuman crudHuman = new CrudHumanImpl();
        List<Human> humans = new ArrayList<>();
        ResultSet resultSet = crudHuman.getAllHumans();
        int i = 0;
        while (resultSet.next()) {
           humans.add(new Human(
                   resultSet.getInt("id"),
                   resultSet.getString("name"),
                   resultSet.getString("lastname"),
                   resultSet.getString("patronymic"),
                   resultSet.getString("city"),
                   resultSet.getString("house"),
                   resultSet.getString("street"),
                   resultSet.getString("flat"),
                   resultSet.getString("numberpassport")));
            System.out.println(humans.get(i));
            i++;
        }
        Human human1 = new Human();
        human1.setName("Серега");
        human1.setLastName("Seregin");
        human1.setPatronymic("Seregovich");
        human1.setCity("Oreyl");
        human1.setStreet("Orlovskaya");
        human1.setHouse("45");
        human1.setFlat("78");
        human1.setNumberPassport("7505 658492");
        crudHuman.createHuman(human1);
        System.out.println("-----------------------------");
        Human human = crudHuman.getHumanById(7);
        System.out.println(human);
        System.out.println("------------------------------");
        human.setName("Stefany");
        crudHuman.updateHuman(human);
        crudHuman.deleteHuman(human);
    }
}
