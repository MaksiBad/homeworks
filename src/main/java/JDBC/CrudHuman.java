package JDBC;
import java.sql.ResultSet;
import java.sql.SQLException;
public interface CrudHuman {
    ResultSet getAllHumans() throws SQLException; //Возвращает список всех людей из базы данных
    Human getHumanById(int id) throws SQLException; //Возвращает конкретного человека, у которого определенный id
    void createHuman(Human human) throws SQLException; //Создает человека и записывает его в БД
    void updateHuman(Human human) throws SQLException; //Обновляет данные по конкретному человеку
    void deleteHuman(Human human) throws SQLException; //Удаляет конкретного человека
}